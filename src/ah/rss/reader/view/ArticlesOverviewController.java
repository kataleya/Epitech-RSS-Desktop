package ah.rss.reader.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.time.LocalDate;
import ah.rss.reader.MainApp;
import ah.rss.reader.model.Articles;

public class ArticlesOverviewController {
	@FXML
	private TableView<Articles> articlesTable;
	@FXML
	private TableColumn<Articles, String> titleColumn;
	@FXML
	private TableColumn<Articles, String> authorColumn;
	@FXML
	private TableColumn<Articles, String> linkColumn;
	@FXML
	private TableColumn<Articles, LocalDate> publishedColumn;
	@FXML
	private TableColumn<Articles, Boolean> readColumn;
	@FXML
	private TableColumn<Articles, Boolean> favoriteColumn;

	@FXML
	private Label titleLabel;
	@FXML
	private Label authorLabel;
	@FXML
	private Label linkLabel;
	@FXML
	private Label publishedLabel;
	@FXML
	private Label readLabel;
	@FXML
	private Label favoriteLabel;
	@FXML
	private SplitPane articlesPane;

	
	// Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ArticlesOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the articles table with two columns.
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().getTitleProperty());
        authorColumn.setCellValueFactory(cellData -> cellData.getValue().getAuthorProperty());
        linkColumn.setCellValueFactory(cellData -> cellData.getValue().getSourceProperty());
        publishedColumn.setCellValueFactory(cellData -> cellData.getValue().getPubDateProperty());
        favoriteColumn.setCellValueFactory(cellData -> cellData.getValue().getFavoriteProperty());
        readColumn.setCellValueFactory(cellData -> cellData.getValue().getReadProperty());
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
		System.out.println(mainApp.getArticles().toString());
		articlesTable.setItems(mainApp.getArticles());
    }
}
