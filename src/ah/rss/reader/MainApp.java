package ah.rss.reader;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.time.LocalDate;

import ah.rss.reader.model.Articles;
import ah.rss.reader.view.ArticlesOverviewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
//import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private VBox rightBox;
	private ObservableList<Articles> articlesData = FXCollections.observableArrayList();

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("AH RSS reader");
		try {
			initRootLayout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public MainApp() {
	  articlesData.add(new Articles("First article", "Mc ct","www.qwant.com", "Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category", "www.mooncul.com", LocalDate.now(), "thisshouldbeahashqs32d1a6dqs3d" ));
	  articlesData.add(new Articles("Second article","Mc ct","www.qwant.com","Total Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category","www.mooncul.com", LocalDate.now(), "thisshouldbeahashqs32d1a6dqs3das"));
	}
	
	  public void initRootLayout() throws IOException {
	    FXMLLoader loader = new FXMLLoader();
		try {
			loader.setLocation(MainApp.class.getResource("view/ReaderView.fxml"));
		    rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
//            rightBox = (VBox) scene.lookup("#rightBox");
//			showArticleTableView();
			primaryStage.setScene(scene);
			primaryStage.show();
			ArticlesOverviewController articlesController = loader.getController();
			articlesController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println(rootLayout.toString());
		//Give the controller access to the main app

		// Show the scene containing the root layout.
	  }

	  /**
	     * Shows the article overview inside the root layout.
	     */
//	    public void showArticleTableView() {
//	        try {
//	            // Load person overview.
//	            FXMLLoader loader = new FXMLLoader();
//	            loader.setLocation(MainApp.class.getResource("view/ArticlesOverview.fxml"));
//	            SplitPane articlesOverview = (SplitPane) loader.load();
//
//	            System.out.println(articlesOverview.toString());
//	            // Set person overview into the center of root layout.
//	            // max => a set dans le splitpane 
//	            rightBox.getChildren().add(articlesOverview);
//	            //	            rootLayout.setCenter(articlesOverview);
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        }
//	    }
//	    
	  public ObservableList<Articles> getArticles() {
		return articlesData;
	  }


	  public Stage getPrimaryStage() {
        return primaryStage;
	  }
	
	  public static void main(String[] args) {
		launch(args);
	  }
}
