package ah.rss.reader.model;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;

/**
 * 
 * Model class for a Feeds
 * @author maxime
 *
 */
public class Feeds {

	private final StringProperty title;
	private final StringProperty description;
	private final StringProperty imageUrl;
	private final StringProperty link;
	private final StringProperty source;
	private final StringProperty language;
	private final StringProperty copyright;
	private final ObjectProperty<LocalDate> lastBuildDate;
	private final StringProperty uid;
	private final IntegerProperty currentOffset;
//	private final StringProperty ttl;
//	private final StringProperty recvUnixDate;
	
	/**
	 * Default constructor
	 */
	public Feeds() {
		this(null, null, null, null, null, null, null, null, null, null);
	}
	
	/**
	 * Param constructor
	 */
	public Feeds(String title, String description, String imageUrl, String link, String source, String language, String copyright, LocalDate lastBuildDate, String uid, Integer currentOffset) {
		this.title = new SimpleStringProperty(title);
		this.description = new SimpleStringProperty(description);
		this.imageUrl = new SimpleStringProperty(imageUrl);
		this.link = new SimpleStringProperty(link);
		this.source = new SimpleStringProperty(source);
		this.language = new SimpleStringProperty(language);
		this.copyright = new SimpleStringProperty(copyright);
		this.lastBuildDate = new SimpleObjectProperty<LocalDate>(lastBuildDate);
		this.uid = new SimpleStringProperty(uid);
		this.currentOffset = new SimpleIntegerProperty(currentOffset);
	}

	
	/**
	 * 
	 * 
	 * Property getters
	 */
	public IntegerProperty getCurrentOffsetProperty() {return currentOffset;}
	public StringProperty getTitleProperty() {return title;}
	public StringProperty getDescriptionProperty() {return description;}
	public StringProperty getImageUrlProperty() {return imageUrl;}
	public StringProperty getSourceProperty() {return source;}
	public StringProperty getLinkProperty() {return link;}
	public StringProperty getLanguageProperty() {return language;}
	public StringProperty getUidProperty() {return uid;}
	public ObjectProperty<LocalDate> getLastBuildDateProperty() {return lastBuildDate;}
	public StringProperty getCopyrightProperty() {return copyright;}


	/**
	 * 
	 * Typed getters
	 * 
	 */
	public int getCurrentOffset() {return currentOffset.get();}
	public String getTitle() {return title.get();}
	public String getDescription() {return description.get();}
	public String getImageUrl() {return imageUrl.get();}
	public String getSource() {return source.get();}
	public String getLink() {return link.get();}
	public String getLanguage() {return language.get();}
	public String getUid() {return uid.get();}
	public LocalDate getLastBuildDate() {return lastBuildDate.get();}
	public String getCopyright() {return copyright.get();}

	
	/**
	 * 
	 * Setters
	 */
	public void setCurrentOffset(int offset) {this.currentOffset.set(offset);}
	public void setTitle(String title) {this.title.set(title);}
	public void setDescription(String description) {this.description.set(description);}
	public void setImageUrl(String imageUrl) {this.imageUrl.set(imageUrl);}
	public void setSource(String source) {this.source.set(source);}
	public void setLink(String link) {this.link.set(link);}
	public void setLanguage(String language) {this.language.set(language);}
	public void setUid(String uid) {this.uid.set(uid);}
	public void setLastBuildDate(LocalDate lastBuildDate) {this.lastBuildDate.set(lastBuildDate);}
	public void setCopyright(String copyright) {this.copyright.set(copyright);}

}
